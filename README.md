
### Overview

This is a sample/template for an AWS lambda with the following features 

+ GitLab CI/CD shell (template deployes to a single environment, more can be added)
+ Creates required IAM permissions for consuming records from a kinesis stream
