REPOSITORY=912528721902.dkr.ecr.ap-southeast-2.amazonaws.com

ifdef CI
	ASSUME_REQUIRED?=assumeRole
endif

build: .env
	@echo "start build"
	docker-compose run --rm --entrypoint 'sh' npm '-c' 'apk add --no-cache make gcc g++ python && npm install'

test: .env
	@echo "start test"
	docker-compose run --rm --entrypoint 'sh' npm '-c' 'npm run test'

deploy: .env $(ASSUME_REQUIRED)
	@echo "start deploy $(env)"
	docker-compose run --rm --entrypoint 'sh' npm '-c' './node_modules/serverless/bin/serverless.js deploy --stage $(env) --force'
	
.env:
	@echo "+++ :env"
	touch .env
	docker-compose run --rm envvars envfile --overwrite

assumeRole: .env
	@echo "+++ :assumeRole"
	docker-compose run --rm aws sh -c '/opt/scripts/assume-role.sh >> .env'
	@echo "assume role done"